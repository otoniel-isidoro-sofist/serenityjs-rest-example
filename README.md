The Report: [Serenity BDD Report](https://baasie.gitlab.io/serenityjs-rest-example/)

This is an example project of how to use [SerenityJS](http://serenity-js.org/) for rest projects with cucumber.

You need the following npm modules to get serenity-js rest to work:  

 @serenity-js/core -> The core of serenity  
 @serenity-js/cucumber -> the cucumber adapter to be able to report  
 serenity-cli -> the CLi that created the report  
 ts-node -> Typescript transpiler  
 cucumber -> cucumber itself  
 typescript -> to work with typescript  
 
You can also just copy paste the dev dependencies I use, which also contains chai en tslint

Then you need to tell serenityjs to use the BDDReporter, I did that in the feature/support/serenity_config.ts file  

For it to run correct you need to --require the @serenity-js/cucumber package and the serenity config:
--require features --require ./node_modules/@serenity-js/cucumber/register.js

That is it!